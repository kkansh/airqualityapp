import { INDEXCOLOR } from "./constants";

export const getColorCode = (data) => {
  var color = [];
  data.forEach((x) => {
    let aqi = x.aqi;
    switch (true) {
      case aqi <= 50:
        color.push(INDEXCOLOR["GOOD"]);
        break;
      case aqi >= 51 && aqi <= 100:
        color.push(INDEXCOLOR["SATISFACTORY"]);
        break;
      case aqi >= 101 && aqi <= 200:
        color.push(INDEXCOLOR["MODERATE"]);
        break;
      case aqi >= 201 && aqi <= 300:
        color.push(INDEXCOLOR["POOR"]);
        break;
      case aqi >= 301 && aqi <= 400:
        color.push(INDEXCOLOR["VERYPOOR"]);
        break;
      case aqi >= 401 && aqi <= 500:
        color.push(INDEXCOLOR["SEVERE"]);
        break;
    }
  });
  return color;
};
