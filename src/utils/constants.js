export const INDEXCOLOR = {
  GOOD: "rgba(85, 168, 79, 255)",
  SATISFACTORY: "rgba(163, 200, 83, 255)",
  MODERATE: "rgba(255, 248, 51, 255)",
  POOR: "rgba(242, 156, 51, 255)",
  VERYPOOR: "rgba(233, 63, 51, 255)",
  SEVERE: "rgba(175, 45, 36, 255)",
};
