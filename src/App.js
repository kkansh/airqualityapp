import logo from "./logo.svg";
import legend from "./resource/img/legends.png";
import "./App.css";
import React from "react";
import PollutionChart from "./component/PollutionChart.js";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <p> Air Quality Control</p>
        <div class="container-header">
          <p class="legend">
            <a href="#">
              <img src={legend} Style="max-width: 300px;" alt="" />
              <div class="text">Chart legend</div>
            </a>
          </p>
        </div>
        <div class="chart-container">
          <PollutionChart />
        </div>
      </header>
    </div>
  );
}

export default App;
