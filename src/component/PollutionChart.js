import React, { useRef, useEffect, useState } from "react";
import { Bar } from "react-chartjs-2";
import { getColorCode } from "../utils/util.js";
import { CONFIG } from "../config/config.js";
const wsa = CONFIG.service.airQuality;
const ws = new WebSocket(wsa);
export default () => {
  const [data, setData] = useState([]);

  // var data = [
  //   {
  //     city: "Mumbai",
  //     aqi: 181.21874173082094,
  //   },
  //   {
  //     city: "Kolkata",
  //     aqi: 203.42855865609408,
  //   },
  //   {
  //     city: "Bhubaneswar",
  //     aqi: 100.10518518583618,
  //   },
  //   {
  //     city: "Chennai",
  //     aqi: 141.237617400759,
  //   },
  //   {
  //     city: "Pune",
  //     aqi: 218.6818481504013,
  //   },
  //   {
  //     city: "Hyderabad",
  //     aqi: 201.56688599038878,
  //   },
  //   {
  //     city: "Indore",
  //     aqi: 52.46738092872897,
  //   },
  //   {
  //     city: "Jaipur",
  //     aqi: 142.8230816744833,
  //   },
  //   {
  //     city: "Chandigarh",
  //     aqi: 45.20869410811825,
  //   },
  //   {
  //     city: "Lucknow",
  //     aqi: 76.02649312512672,
  //   },
  // ];
  const [config, setConfig] = useState(null);
  useEffect(() => {
    ws.onopen = () => {
      // on connecting, do nothing but log it to the console
      console.log("connected");
    };

    ws.onmessage = (evt) => {
      // listen to data sent from the websocket server
      const message = JSON.parse(evt.data);
      setData(message);
      console.log(message);
    };

    ws.onclose = () => {
      console.log("disconnected");
      // automatically try to reconnect on connection loss
    };
    setConfig({
      labels: data.map((x) => x.city),
      datasets: [
        {
          label: "AQI",
          backgroundColor: getColorCode(data),
          borderColor: getColorCode(data),
          borderWidth: 2,
          data: data.map((x) => x.aqi),
        },
      ],
    });
  }, [data]);

  return (
    <div>
      <Bar
        data={config}
        options={{
          indexAxis: "y",
          title: {
            display: true,
            text: "Air Quality Index",
            fontSize: 30,
          },
          legend: {
            display: true,
            position: "right",
          },
        }}
      />
    </div>
  );
};
